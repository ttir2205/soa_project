var mysql = require('mysql');
  
  var pool  = mysql.createPool({
    host     : 'db',
    user     : 'root',
    password : 'cfr1907cluj',
    database : 'exchange'
  });
  
  
  class Mysql {
      constructor () {
        
      }

      queryInsertCurrency(name, country) {
        return new Promise((resolve, reject) => {
            pool.query('INSERT into currency (name, country) values (?, ?)', [name, country], function (error, results, fields) {
                if (error) {
                    throw error
                };
                resolve(results)
            });
          });
      }

      queryGetCurrencies() {
        return new Promise((resolve, reject) => {
          pool.query('SELECT * from currency', function (error, results, fields) {
              if (error) {
                  throw error
              };
              resolve(results)
          });
        });
      }

      queryDeleteCurrency(name) {
        return new Promise((resolve, reject) => {
          pool.query('DELETE from currency where name = ?', [name], function (error, results, fields) {
              if (error) {
                  throw error
              };
              resolve(results)
          });
        });
      }
  }



const Koa = require('koa');
const Router = require('@koa/router');
const koaBody = require('koa-body');
const cors = require('@koa/cors');

const port = 5002;

const router = new Router();

const app = new Koa();

const db = new Mysql();

app.use(cors());
app.use(koaBody());


router.post('/currency', async ctx => {
    ctx.status = 200;
    const { name, country } = ctx.request.body;

    query = await db.queryInsertCurrency(name, country);
    
    currency = {name, country}
    
    ctx.body = currency;
});



router.get('/currency', async ctx => {
    try {
        ctx.status = 200;
        const { username, password, firstName, lastName } = ctx.request.body;

        const currencies = await db.queryGetCurrencies(username, password, firstName, lastName);
        
        ctx.body = currencies;
    } catch (ex) {
        ctx.status = 400;
    }
});


router.delete('/currency/:name', async ctx => {
    try {
        const name = ctx.params.name;
        ctx.status = 200;

        await db.queryDeleteCurrency(name);
        
        ctx.body = null;
    } catch (ex) {
        ctx.status = 400;
    }
});

app.use(router.routes())
    .use(router.allowedMethods());

app.listen(port);
console.log(`Listening on port ${port}.`);