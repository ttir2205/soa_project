var mysql = require('mysql');
  
  var pool  = mysql.createPool({
    host     : 'db',
    user     : 'root',
    password : 'cfr1907cluj',
    database : 'exchange'
  });
  
  
  class Mysql {
      constructor () {
        
      }
      queryFindUser (username, password) {
        return new Promise((resolve, reject) => {
          pool.query('SELECT * from userr where username = ? and password = ?', [username, password], function (error, results, fields) {
              if (error) {
                  throw error
              };
              resolve(results)
          });
        });
      }

      queryRegister(username, password, firstName, lastName) {
        return new Promise((resolve, reject) => {
            pool.query('INSERT into userr (username, password, first_name, last_name, role) values (?, ?, ?, ?, ?)', [username, password, firstName, lastName, 'user'], function (error, results, fields) {
                if (error) {
                    throw error
                };
                resolve(results)
            });
          });
      }
  }


const Koa = require('koa');
const Router = require('@koa/router');
const koaBody = require('koa-body');
const jwt = require('jsonwebtoken');
const cors = require('@koa/cors');

const port = 5001;

const router = new Router();
const secret = 'my-secret';

const app = new Koa();

const db = new Mysql();

app.use(cors());
app.use(koaBody());


router.post('/login', async ctx => {
    ctx.status = 200;
    const { username, password } = ctx.request.body;

    const query = await db.queryFindUser(username, password);
    const user = query[0];
    
    if (user) {
        ctx.body = { token: jwt.sign(ctx.request.body, secret), user };
    } else {
        ctx.status = 403;
    }
});


router.post('/register', async ctx => {
    try {
        ctx.status = 200;
        const { username, password, firstName, lastName } = ctx.request.body;

        await db.queryRegister(username, password, firstName, lastName);
        user = {username, password, firstName, lastName};
        
        ctx.body = user;
    } catch (ex) {
        ctx.status = 400;
    }
});


router.post('/authorize', async ctx => {
    ctx.status = 200;
    try {
        const { username, password } = jwt.verify(ctx.request.body.token, secret);

        const query = await db.queryFindUser(username, password);
        const user = query[0];

        if (user) {
            ctx.body = { authorized: true };
        } else {
            ctx.status = 401;
            ctx.body = { authorized: false };
        }
    } catch (err) {
        ctx.status = 401;
    }
})


app.use(router.routes())
    .use(router.allowedMethods());

app.listen(port);
console.log(`Listening on port ${port}.`);