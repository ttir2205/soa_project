import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from "src/environments/environment";
import { catchError, mergeMap } from "rxjs/operators";
import { EMPTY } from "rxjs";
import { Router } from "@angular/router";

@Injectable()
export class RepositoryService {

    constructor(private readonly httpClient: HttpClient, private readonly router: Router) {}

    public getData (port: number, route: string, headers?: HttpHeaders) {
        return this.authorizeUser().pipe(
            mergeMap(() => this.httpClient.get(this.createCompleteRoute(port, route, environment.urlAddress), this.generateHeaders(headers)))
        );
    }

    public create (port: number, route: string, body: any, headers?: HttpHeaders) {
        return this.authorizeUser().pipe(
            mergeMap(() => this.httpClient.post(this.createCompleteRoute(port, route, environment.urlAddress), body, this.generateHeaders(headers)))
        );
    }

    public update (port: number, route: string, body: any, headers?: HttpHeaders) {
        return this.authorizeUser().pipe(
            mergeMap(() => this.httpClient.put(this.createCompleteRoute(port, route, environment.urlAddress), body, this.generateHeaders(headers)))
        );
    }

    public delete (port: number, route: string, headers?: HttpHeaders) {
        return this.authorizeUser().pipe( 
            mergeMap(() => this.httpClient.delete(this.createCompleteRoute(port, route, environment.urlAddress), this.generateHeaders(headers)))
        );
    }


    private createCompleteRoute (port: number, route: string, envAddress: string) {
        return `${envAddress}${port}/${route}`;
    }

    private authorizeUser() {
        return this.httpClient.post("http://localhost:1234/authorize", {"token" : localStorage.getItem("token")}, this.generateHeaders()).pipe(catchError((err) => {
            alert("You are not authorized to perform this action");
            this.router.navigate(["/login"]);
            return EMPTY;
        }));
    }

    private generateHeaders(headers?: HttpHeaders) {
        if (headers == null || headers == undefined) {
            headers = new HttpHeaders();
        }

        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Access-Control-Allow-Origin', '*');

        return {
            headers: headers
        }
    }

}