import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { RepositoryService } from "../services/repository.service";
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm : FormGroup = new FormGroup({});

  constructor(
    private router: Router, 
    private readonly httpClient: HttpClient) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup( {
      username: new FormControl(''),
      password: new FormControl('')
    });
  }

  public loginUser(loginFormValue: any) {
    this.httpClient.post("http://localhost:1234/login", loginFormValue, { observe: 'response' }).subscribe((res : any) => {
      const userBody = res.body;
      localStorage.setItem("token", userBody.token);
      if (userBody.user.role === "admin") {
        this.router.navigate(["/admin"]);
      } else {
        this.router.navigate(["/user"]);
      }
    }, () => {
      alert("Wrong username or password");
    });
  }

  public openRegister() {
    this.router.navigate(["/register"]);
  }

}
