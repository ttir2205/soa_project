import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public registerForm : FormGroup = new FormGroup({});

  constructor(private readonly httpClient: HttpClient, private router: Router) { }

  ngOnInit(): void {
    this.registerForm = new FormGroup( {
      username: new FormControl(''),
      password: new FormControl(''),
      firstName: new FormControl(''),
      lastName: new FormControl(''),
    });
  }

  public registerUser(registerFormValue: any) {
    this.httpClient.post("http://localhost:1234/register", registerFormValue, { observe: 'response' }).subscribe((res : any) => {
      if (res) {
        this.router.navigate(["/login"]);
      }
    })
  }

  public openLogin() {
    this.router.navigate(["/login"]);
  }

}
