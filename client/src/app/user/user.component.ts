import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { RepositoryService } from "../services/repository.service";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  public currencies: any[] = [];
  public exchange: any;

  private PORT_NUMBER = 5003;
  

  constructor(
    private readonly repositoryService: RepositoryService,
    private readonly router: Router
  ) { }


  ngOnInit(): void {
    this.repositoryService.getData(this.PORT_NUMBER, "currency").subscribe((currencies: any) => {
      this.currencies = currencies;
    });
  }


  public onSelectCurrency(event: any) {
    var currency = event.target.value;
    this.repositoryService.getData(this.PORT_NUMBER,`exchange?name=${currency}`).subscribe((exchange: any) => {
      this.exchange = exchange;
    })
  }


  public logOut() {
    localStorage.removeItem("token");
    this.router.navigate(["/login"]);
  }


}
