import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { RepositoryService } from "../services/repository.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  public currencies: any[] = [];
  public currencyForm: FormGroup = new FormGroup({});

  private PORT_NUMBER = 5002;


  constructor(
    private readonly repositoryService: RepositoryService,
    private readonly router: Router
  ) { }


  ngOnInit(): void {
    if (!localStorage.getItem("token")) {
      this.router.navigate(["/login"])
    }

    this.currencyForm = new FormGroup( {
      name: new FormControl(''),
      country: new FormControl(''),
    });

    this.repositoryService.getData(this.PORT_NUMBER, "currency").subscribe((currencies: any) => {
      this.currencies = currencies;
    });
  }


  addCurrency(formValue: any) {
    let body = {
      name: formValue.name,
      country: formValue.country
    }
    
    this.repositoryService.create(this.PORT_NUMBER,"currency",body).subscribe((currency: any) => {
      alert("Successfully added currency!");
      this.currencies.push(currency);
      this.currencyForm.reset();
    }, () => {
      alert("An error occured when adding the currency!");
    });
  }


  public deleteCurrency(name: string) {
    this.repositoryService.delete(this.PORT_NUMBER,`currency/${name}`).subscribe(() => {
      alert("Successfully deleted currency!");
      const currencyIndex = this.currencies.findIndex(currency => currency.name === name);
      this.currencies.splice(currencyIndex, 1);
    }, () => {
      alert("An error occured when deleting the currency!");
    });
  }


  public logOut() {
    localStorage.removeItem("token");
    this.router.navigate(["/login"]);
  }

}
