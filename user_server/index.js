var mysql = require('mysql');
  
  var pool  = mysql.createPool({
    host     : 'db',
    user     : 'root',
    password : 'cfr1907cluj',
    database : 'exchange'
  });
  
  
  class Mysql {
      constructor () {
        
      }

      queryGetCurrencies() {
        return new Promise((resolve, reject) => {
          pool.query('SELECT * from currency', function (error, results, fields) {
              if (error) {
                  throw error
              };
              resolve(results)
          });
        });
      }
  }


const Koa = require('koa');
const Router = require('@koa/router');
const koaBody = require('koa-body');
const cors = require('@koa/cors');
const fetch = require('node-fetch');

const port = 5003;

const router = new Router();

const app = new Koa();

const db = new Mysql();

app.use(cors());
app.use(koaBody());


router.get('/exchange', async ctx => {
    const { name } = ctx.request.query;
    if (name) {
        const exchange = await fetch(`https://api.exchangeratesapi.io/latest?base=${name}`, { method: 'GET' }).then(res => res.json());
        var rates = [];
        Object.keys(exchange.rates).map(key => rates.push({"name":key, "value": exchange.rates[key]}));
        exchange.rates = rates;
        ctx.response.body = exchange;
    } else {
        ctx.response.body = {};
    }
});



router.get('/currency', async ctx => {
    try {
        ctx.status = 200;
        const { username, password, firstName, lastName } = ctx.request.body;

        const currencies = await db.queryGetCurrencies(username, password, firstName, lastName);
        
        ctx.body = currencies;
    } catch (ex) {
        ctx.status = 400;
    }
});


app.use(router.routes())
    .use(router.allowedMethods());

app.listen(port);
console.log(`Listening on port ${port}.`);